var regex = /[?&]([^=#]+)=([^&#]*)/g,
	url = window.location.href,
	params = {},
	match;
while(match = regex.exec(url)) {
	params[match[1]] = match[2];
}


var endPoint = "https://wap.tplinkcloud.com?token="+params.token;

function errorPage(data) {
	var errorArea = document.getElementById('errorresult');
	if (errorArea) {
		errorArea.innerText = JSON.stringify(data);
		errorArea.classList = "";
	}
}

var latestConfig = {
	devices : []
};

function createConfig(data) {
	var configjson = document.getElementById('configjson');
	var config = {
		devices : []
	};
	data.result.deviceList.forEach(function(item){
		config.devices.push({name:item.alias, deviceId:item.deviceId, type:item.deviceModel, url:item.appServerUrl});
	});
	latestConfig = config;
	//configjson.innerText = JSON.stringify(config, null, 4);
	//configjson.classList = "";
	var writeButton = document.getElementById('writeButton');

	writeButton.onclick=function(){makeXHRRequest('writeConfig', latestConfig, function() {console.log('Config written');window.alert('config.json written');});};
}

var deviceData = {};

function deviceState(item, state) {
	var device = deviceData.result.deviceList[item];
	var payload = {"method": "passthrough","params": {"deviceId": device.deviceId, "requestData": "{\"system\":{\"set_relay_state\":{\"state\":"+state+"} } }"}};
	makeXHRRequest (device.appServerUrl+'?token='+params.token, payload, function(){console.log('DONE!');});
}

function createRow(data) {
	var row=document.createElement('tr');
	data.forEach(function (item){
		var tdNode = document.createElement('td');
		tdNode.innerHTML=item;
		row.appendChild(tdNode);
	});
	return row;
}

function parseResult(data) {
	var parentItem = document.getElementById('deviceList');

	data.result.deviceList.forEach(function(device, item){
		parentItem.appendChild(createRow([device.alias, device.deviceId, device.status, device.deviceModel, device.appServerUrl, '[<a href="javascript:deviceState('+item+', 1);">on</a>] [<a href="javascript:deviceState('+item+', 0);">off</a>]']));
	});

	parentItem.className='';
}

function executeList() {
	makeXHRRequest(endPoint, {"method": "getDeviceList"}, function(data) {
		if (data.error_code !== 0) {
			errorPage(data);
			return;
		} else {
			deviceData = data;
			createConfig(data);
			parseResult(data);
		}
		console.log(JSON.stringify(data));
	});
}

function makeXHRRequest(url, payload, continuation) {
	var xhr = new XMLHttpRequest();
	xhr.open("POST", url, true);
	xhr.setRequestHeader("Content-type", "application/json");
	xhr.onreadystatechange = function (data) {
		if (xhr.readyState === 4 && xhr.status === 200) {
			var json = JSON.parse(xhr.responseText);
			continuation(json);
		}
	};
	var data = JSON.stringify(payload);
	xhr.send(data);
}

executeList();



