var express = require('express');
var router = express.Router();

var path = require('path');
var filesys = require('fs');
var https = require('https');
var querystring = require('querystring');

var fileserver = require('./fileserver');


// var app = express();
//
// app.use(session({
// 	secret: 'IMFUCKINGBATMAN',
// 	resave: true,
// 	saveUninitialized: false /*, cookie: { maxAge: 60000 }*/
// }));

router.use('/*', doFilter);
//router.get('/*', doFilter);
//router.post('/*', doFilter);

var pathDefault = "../..";

function writeFile(filename, configData, callback) {
	var fs = require('fs');
	fs.writeFile(filename, JSON.stringify(configData, null, 4), function (err) {
		if (err) {
			callback(err);
		}
		console.log("The file was saved!");
		callback();
	});
}

function writeConfig(request, response) {
	writeFile('config.json',request.body, function(err){
		response.writeHeader(200, {
			"Content-Type": "application/json"
		});
		if (!err) {
			console.log('write successful');
			response.write(JSON.stringify('{"result":"ok"}'));
		} else {
			console.log('write fail. Err: '+err);
			response.write(JSON.stringify('{"result":"error"}'));
		}
		response.end();
	});
}

var deviceList = {
	Lights : {
		'FrontRoom': {
			deviceId : 'xxxxxx',
			connectionType : 'HS100'
		},
		'Bedroom' : {
			deviceId : 'xxxxxx',
			connectionType : 'HS100'
		}
	}
};

function ext(url) {
	return (url = url.substr(1 + url.lastIndexOf("/")).split('?')[0]).split('#')[0].substr(url.lastIndexOf(".")+1);
}

function justfilename(url) {
	return (url = url.substr(1 + url.lastIndexOf("/")).split('?')[0]).split('#')[0];
}

function doFilter(request, response) {
	//Check if a file is being requested or not
	var pageurl = ext(request.baseUrl).toLowerCase();
	if (pageurl.match(/(html|css|png|jpg|htm|js)/g)) {
		//stuff
		fileserver.sendFile(response, './files/TP/'+justfilename(request.baseUrl));
	} else {
		switch (justfilename(request.baseUrl.toLowerCase())) {
			case '/gettoken' :
			case 'gettoken' :
				getToken(request, response);
				break;
			case '/writeconfig' :
			case 'writeconfig' :
				writeConfig(request, response);
				break;
			default:
				doList(request, response);
		}

	}

}

function getToken(request, response) {
	var loginPayload = {
		"method": "login",
		"params": {
			"appType": "Kasa_Android",
			"cloudUserName": request.body.username,
			"cloudPassword": request.body.password,
			"terminalUUID": request.body.UUID
		}
	};

	makeXHRRequest({host:"wap.tplinkcloud.com", path:""}, loginPayload, function(data){
		response.writeHeader(200, {
			"Content-Type": "text/html"
		});
		if (data && data.hasOwnProperty('error_code') && data.error_code === 0) {
			response.write('<html><head><title>Token</title></head><body>Token obtained.  Payload result: <br/>');
			response.write(JSON.stringify({"token":data.result.token}));
			response.write('<br/><br/><a href="deviceList.html?token='+data.result.token+'">Click here to jump straight to device list page</a></body></html>');
		} else {
			response.write('<html><head><title>Error</title></head><body>Error.  Payload result: <br/>');
			response.write(JSON.stringify(data));
			response.write('</body></html>');
		}
		response.end();
	});
}

function makeXHRRequest(theoptions, payload, continuation) {

	var postData = JSON.stringify(payload);
	// host: endPoint,
	// 	path: '/?token='+request.query.token,
	var options = {
		host: theoptions.host,
		path: theoptions.post,
		method: 'POST',
		headers: {
			'Content-Type' : 'application/json',
			'Content-Length': Buffer.byteLength(postData)
		}
	};
	console.log('connecting to https://'+options.host+options.path);
	var req = https.request(options, function(res){
		var bodyChunks = [];
		res.setEncoding('utf8');
		res.on('data', function(chunk) {
			// You can process streamed parts here...
			bodyChunks.push(chunk);
		}).on('end', function() {
			var body = bodyChunks.reduce(function(first, second) {return first+second;});
			console.log('BODY: ' + body);
			// ...and/or process the entire body here.
			continuation(JSON.parse(body));
		})
	});
	req.write(postData);
	console.log('Transmitting: ['+Buffer.byteLength(postData)+']'+postData);
	req.end();
}

function doList(request, response) {
	//first parse data
	var params = request.query;//filterquerystring(request.query.location.search);

	var endPoint = "wap.tplinkcloud.com";

	var commandList = [];
	function clearCommands() {
		commandList = [];
	}

	function addCommand(group, device, state) {
		commandList.push({deviceId: deviceList[group][device].deviceId, state: state});
	}

	function executeList() {
		if (commandList.length > 0) {
			var commandAction = commandList.pop();
			makeXHRTrigger({host:endPoint, path: '/?token='+request.query.token}, {deviceId : commandAction.deviceId, state:commandAction.state}, executeList);
		} else if (doneFunction) {
			doneFunction();
		}
	}

	function makeXHRTrigger(theoptions, payload, continuation) {
		var triggerReq = '{"method": "passthrough","params": {"deviceId": payload.deviceId, "requestData": "{\"system\":{\"set_relay_state\":{\"state\": "+payload.state+"} } }"}}';
		makeXHRRequest(theoptions, triggerReq, continuation);
	}

	response.writeHeader(200, {
		"Content-Type": "application/json"
	});
	if (params.commands) {
		try {
			var commandData = JSON.parse('{"data":'+decodeURIComponent(params.commands)+'}');
			commandData.data.forEach(function (command) {
				addCommand(command.group, command.deviceId, command.state);
			});
			doneFunction = function() {
				response.write(JSON.stringify('{"result":"ok"}'));
				response.end();
			};
			executeList();
		} catch (e) {
			response.write(JSON.stringify('{"result":"error"}'));
			response.end();
		}
	} else {
		response.write(JSON.stringify('{"result":"error"}'));
		response.end();
	}
}

function filterquerystring(querystring) {
	querystring = querystring.split('&');
	var params = {};
	querystring.forEach(function(item){
		item=item.split('=');
		params[item[0]]=item[1];
	});
	return params;
}

module.exports = router;