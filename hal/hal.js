//Requires
var express = require('express');
var app = express();

var path = require("path");
var session = require('express-session');
var filesys = require("fs");
var url = require("url");

var tplink = require('./tplink');


// Use the session middleware
app.use(session({ secret: 'SHHHHHHHH', resave:true, saveUninitialized:false })); //Separate 'secret' to the GLR server

// ---- Config parsing + rewriting -------------------------------------------------------------------------------------

var bodyParser = require('body-parser');
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
	extended: true
}));

var router = express.Router();

app.use('/HAL/TP', tplink); //GLR Server
app.listen(9000, function () {
	console.log('HAL listening on localhost port 9000.');
	console.log('baseUrl: /HAL/TP');
});