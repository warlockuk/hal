	var filesys = require("fs");
	var path = require("path");

	function sendFile(response, local_path) {
		sendFileFP(response, path.join(process.cwd(), local_path));
	}

	function sendFileFP(response, full_path) {
		filesys.stat(full_path, function(err, stat) {
			if (err === "EISDIR") {
				//Ugh.  Root folder access attempt.
				response.end();
			} else if (err !== null) {
				response.writeHeader(404, {
					"Content-Type": "text/plain"
				});
				response.write("404 Not Found\n");
				response.end();
			} else {
				filesys.readFile(full_path, "binary", function(err, file) {
					if (err) {
						response.writeHeader(500, {
							"Content-Type": "text/plain"
						});
						response.write(err + "\n");
						response.end();

					} else {
						response.writeHeader(200);
						response.write(file, "binary");
						response.end();
					}

				});
			}
		});
	}

	module.exports = {
		sendFile : sendFile,
		sendFileFP : sendFileFP
	};
