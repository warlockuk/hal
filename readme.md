# HAL #
### Home Automation Library ###

Node webserver which IFTTT can be pointed to.

Currently, if you use IFTTT's services to use a keyword to trigger a webservice it'll only perform one XHR request.

If you want to, say, turn off all lights or activate two lights out of five to create a 'scene' this is not currently possible.

Pre-requisites:

* git
* nodejs
* npm

To use this server:
1) pull down the repository with git -clone https://<yourbitbucketusername>@bitbucket.org/warlockuk/hal.git

If you have trouble at this point, stop.

2) In the hal subfolder where the js files live, there is a config.example.json file.

This is what we'll edit to configure your device IDs for communication, make a copy and rename it to config.json

3) In the root project folder that was just created, do the following:

        npm install
        cd hal
        node hal.js

Now go to your web browser.

3) Get a UUID4 id - go to https://www.uuidgenerator.net/version4 and you'll see a freshly generated UUID key.

Copy this key and go to http://localhost:9000/HAL/TP/getToken.html

On this page you'll see a form.  Past the UUID4 into the form, enter your TPLink account username and password and click submit.  On the page that follows you'll see your login token.  You'll want to make a copy of this. :)

4) If you go to http://localhost:9000/HAL/TP/deviceList.html?token=yourTokenHere you'll see a list of all devices on your account, this data should be copied into the config.json file

5) Go to http://localhost:9000/HAL/TP/reloadConfig.html - this will reload your new config data.  Or you can restart node.

6) Go to http://localhost:9000/HAL/TP/sceneBuilder.html - this page will show all the devices contained in your config.  You can use the on/off switches to turn them on or off, and if you want to get the URL+data to turn a group on/off in IFTTT, just set the devices to on/off as you want them to appear, tick the checkbox next to items you want to change on the call (you're basically creating a scene) and click the generate data button.
 
7) Make your node server accessible.  This means port forwarding to the machine running the application (forwarding port 9000 to the IP) OR port forwarding an actual domain name (or a free noip.org domain) or some kind of Apache server.
    
Personally, I have an apache webserver which also runs node, and then in the website's config (in /etc/apache2/sites-enabled/000-default.conf) I proxy 127.0.0.1:9000 to <mydomain/HAL/TP>

    RewriteEngine on    
    RewriteRule ^/HAL/(.*) http://localhost:9000/HAL/$1 [proxy]
    RewriteRule ^/HAL http://localhost:9000 [proxy]
    
There's a ton of documentation on what is needed to be installed and configured to do this for Apache online so I'll not bother here.

8) Go to https://platform.ifttt.com/

Make an account if you haven't already.  IFTTT's site will also tell you how to connect to your Google account or Alexa account etc.  

* Go to Applets, new Applet
* Under Triggers, pick Google Assistant, 'Say a simple phrase'
* Under 'What do you want to say?' where the value box is, put the word or phrase you want to trigger your 'scene'
* Under Action, pick 'Webhooks'
* Field Label, you'll need:
* http://PORTFORWARDEDIP?token=yourTokenHere
    or http://PORTFORWARDEDIP:9000?token=yourTokenHere
* Method: POST
* Content Type: application/json
* Body: The JSON object from the sceneBuilder page
    
Finish filling the page in, save and then try it out!
    
### TO DO ###
* Speed up requests
* Status reading
* Visual device chart
* Hue integration
